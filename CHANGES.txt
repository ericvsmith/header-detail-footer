Change log
==========

2.4 2016-10-27 Eric V. Smith
----------------------------

* Renamed distribution name to replace hyphens with underscores.  The
  name is now header_detail_footer (issue #7).

* Remove hack for renaming RPMs (issue #5).

* Always require setuptools (issue #4).

* No code changes.

2.3 2014-03-13 Eric V. Smith
----------------------------

* Added MANIFEST.in to MANIFEST.in (issue #2).

* Have bdist_rpm use the package name 'python-header-detail-footer'
  (issue #3).

2.2 2013-12-03 Eric V. Smith
----------------------------

* Add documentation about CSV files.

* Change protocol error from ValueError to RuntimeError. Closes
  Bitbucket issue #1.

2.1 2013-11-16 Eric V. Smith
----------------------------

* Add a MANIFEST.in so non-code files end up in sdist.

2.0 2013-11-15 Eric V. Smith
----------------------------

* Changed API to return a callable only for footer, since that's the
  only thing that needs to be delayed after details are exhausted.

* Changed nomenclature: now refers to "rows" instead of "lines".


1.0 2013-11-15 Eric V. Smith
----------------------------

* First stable version.

0.1 2013-11-14 Eric V. Smith
----------------------------

* Initial release.
